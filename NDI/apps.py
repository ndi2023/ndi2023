from django.apps import AppConfig


class NdiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'NDI'
