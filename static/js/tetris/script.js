var btn_start = document.getElementById('start');
btn_start.onclick = function () {
    playTetris();
};

function playTetris() {
    var start = document.getElementById('start');
    start.style.display = 'none';
    var audio = document.getElementById("audio");
    audio.play();
    let tetris = new Tetris();
    tetris.start();
}

var canva = document.getElementById('canvas');
var ctx = canva.getContext('2d');

ctx.fillStyle = '#000';
ctx.fillRect(0, 0, COLS * BLOCK_SIZE, ROWS * BLOCK_SIZE);
ctx.strokeStyle = '#eee';
for (let i = 0; i < ROWS; i++) {
    ctx.beginPath();
    ctx.moveTo(0, i * BLOCK_SIZE);
    ctx.lineTo(COLS * BLOCK_SIZE, i * BLOCK_SIZE);
    ctx.stroke();
}
for (let i = 0; i < COLS; i++) {
    ctx.beginPath();
    ctx.moveTo(i * BLOCK_SIZE, 0);
    ctx.lineTo(i * BLOCK_SIZE, ROWS * BLOCK_SIZE);
    ctx.stroke();
}



