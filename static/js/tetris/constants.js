var canvas = document.getElementById('canvas');
var height = canvas.clientHeight;
var width = canvas.clientWidth;

canvas.width = width;
canvas.height = height;

const COLS = 10;
const ROWS = 20;
const BLOCK_SIZE = height / ROWS;

var canvas_next = document.getElementById('next_shape');
var height_next = canvas_next.clientHeight;
var width_next = canvas_next.clientWidth;

canvas_next.width = width_next;
canvas_next.height = height_next;

I = new Array(
    [1,1,1,1],
    [0,0,0,0],
    [0,0,0,0]
);
// Piece 2
O = new Array(
    [1,1],
    [1,1]
);
// Piece 3
L = new Array(
    [1,0,0],
    [1,1,1],
    [0,0,0]
);
// Piece 4
J = new Array(
    [0,0,1],
    [1,1,1],
    [0,0,0]
);
// Piece 5
T = new Array(
    [0,1,0],
    [1,1,1],
    [0,0,0]
);
// Piece 6
S = new Array(
    [0,1,1],
    [1,1,0],
    [0,0,0]
);
// Piece 7
Z = new Array(
    [1,1,0],
    [0,1,1],
    [0,0,0]
);
// Tableau des pieces
pieces = new Array();
pieces = [I,O,L,J,T,S,Z];
// Tableau des couleurs
colors = new Array();
colors = ["#00FFFF","#FFFF00","#0000FF","#FFA500","#FF00FF","#00FF00","#FF0000"];
