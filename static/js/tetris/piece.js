class Piece {
    constructor(x, y, color, shape) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.shape = shape;
        this.blocks = [];
        this.blocks = this.createBlocks();
    }
    createBlocks() {
        let blocks = [];
        this.shape.forEach((row, y) => {
            row.forEach((val, x) => {
                if (val) {
                    blocks.push(new Block(this.x + x, this.y + y, this.color));
                }
            });
        });
        return blocks;
    }
    left() {
        for (let i = 0; i < this.blocks.length; i++) {
            this.blocks[i].left();
        }
        this.x--;
    }
    right() {
        for (let i = 0; i < this.blocks.length; i++) {
            this.blocks[i].right();
        }
        this.x++;
    }
    down() {
        for (let i = 0; i < this.blocks.length; i++) {
            this.blocks[i].down();
        }
        this.y++;
    }
    rotateRight() {
        this.shape = this.shape[0].map((val, index) => this.shape.map((row) => row[index]).reverse());
        this.blocks = this.createBlocks();
    }
    rotateLeft() {
        this.shape = this.shape[0].map((val, index) => this.shape.map((row) => row[index])).reverse();
        this.blocks = this.createBlocks();
    }
    canLeft(fixedPieces) {
        for (let i = 0; i < this.blocks.length; i++) {
            if (!this.blocks[i].canLeft(fixedPieces)) {
                return false;
            }
        }
        return true;
    }
    canRight(fixedPieces) {
        for (let i = 0; i < this.blocks.length; i++) {
            if (!this.blocks[i].canRight(fixedPieces)) {
                return false;
            }
        }
        return true;
    }
    canDown(fixedPieces) {
        for (let i = 0; i < this.blocks.length; i++) {
            if (!this.blocks[i].canDown(fixedPieces)) {
                return false;
            }
        }
        return true;
    }
    canRotateRight(fixedPieces) {
        let shape = this.shape[0].map((val, index) => this.shape.map((row) => row[index]).reverse());
        for (let i = 0; i < shape.length; i++) {
            for (let j = 0; j < shape[i].length; j++) {
                if (shape[i][j]) {
                    let x = this.x + j;
                    let y = this.y + i;
                    if (x < 0 || x >= COLS || y < 0 || y >= ROWS) {
                        return false;
                    }
                    if (fixedPieces[y][x]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    canRotateLeft(fixedPieces) {
        let shape = this.shape[0].map((val, index) => this.shape.map((row) => row[index]));
        for (let i = 0; i < shape.length; i++) {
            for (let j = 0; j < shape[i].length; j++) {
                if (shape[i][j]) {
                    let x = this.x + j;
                    let y = this.y + i;
                    if (x < 0 || x >= COLS || y < 0 || y >= ROWS) {
                        return false;
                    }
                    if (fixedPieces[y][x]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    draw(ctx) {
        this.blocks.forEach((block) => {
            block.draw(ctx);
        });
    }
    static getRandomPiece() {
        let index = Math.floor(Math.random() * 7);
        let color = colors[index];
        let shape = pieces[index];
        return new Piece(0, 1, color, shape);
    }
    static pieceToPlay(piece) {
        let color = piece.color;
        let shape = piece.shape;
        return new Piece(4, 0, color, shape);
    }
}
