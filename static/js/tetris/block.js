class Block {
    constructor(x, y, color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }
    left() {
        this.x--;
    }
    right() {
        this.x++;
    }
    down() {
        this.y++;
    }
    canLeft(fixedPieces) {
        let x = this.x - 1;
        let y = this.y;
        return this.check(x, y, fixedPieces);
    }
    canRight(fixedPieces) {
        let x = this.x + 1;
        let y = this.y;
        return this.check(x, y, fixedPieces);
    }
    canDown(fixedPieces) {
        let x = this.x;
        let y = this.y + 1;
        return this.check(x, y, fixedPieces);
    }
    check(x, y, fixedPieces) {
        if (x < 0 || x >= COLS || y < 0 || y >= ROWS) {
            return false;
        }
        if (fixedPieces[y][x]) {
            return false;
        }
        return true;
    }
    draw(ctx) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x * BLOCK_SIZE, this.y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
        ctx.strokeStyle = '#fff';
        ctx.strokeRect(this.x * BLOCK_SIZE, this.y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
    }
}
