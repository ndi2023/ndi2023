class Tetris {
    constructor() {
        this.canvas = document.getElementById('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.nextCanvas = document.getElementById('next_shape');
        this.nextCtx = this.nextCanvas.getContext('2d');
        this.score = document.getElementById('score');
        this.lines = document.getElementById('lines');
        this.scoreValue = 0;
        this.linesValue = 0;
        this.currentPiece = null;
        this.nextPiece = Piece.getRandomPiece();
        this.fixedPieces = [];
        for (let i = 0; i < ROWS; i++) {
            this.fixedPieces[i] = [];
            for (let j = 0; j < COLS; j++) {
                this.fixedPieces[i][j] = null;
            }
        }
        this.timer = null;
        this.isGameOver = false;
        this.isPause = false;
        this.init();
    }
    init() {
        this.score.innerHTML = this.scoreValue;
        this.lines.innerHTML = this.linesValue;
        this.ctx.clearRect(0, 0, COLS * BLOCK_SIZE, ROWS * BLOCK_SIZE);
        this.nextCtx.clearRect(0, 0, 4 * BLOCK_SIZE, 4 * BLOCK_SIZE);
        this.createPiece();
        this.createNextPiece();
        this.bindEvent();
        this.grille();
        this.draw();
    }
    grille() {
        this.ctx.fillStyle = '#000';
        this.ctx.fillRect(0, 0, COLS * BLOCK_SIZE, ROWS * BLOCK_SIZE);
        this.ctx.strokeStyle = '#eee';
        for (let i = 0; i < ROWS; i++) {
            this.ctx.beginPath();
            this.ctx.moveTo(0, i * BLOCK_SIZE);
            this.ctx.lineTo(COLS * BLOCK_SIZE, i * BLOCK_SIZE);
            this.ctx.stroke();
        }
        for (let i = 0; i < COLS; i++) {
            this.ctx.beginPath();
            this.ctx.moveTo(i * BLOCK_SIZE, 0);
            this.ctx.lineTo(i * BLOCK_SIZE, ROWS * BLOCK_SIZE);
            this.ctx.stroke();
        }
    }
    start() {
        this.timer = setInterval(() => {
            this.moveDown();
        }, 1000);
    }
    pause() {
        clearInterval(this.timer);
        this.isPause = true;
    }
    bindEvent() {
        document.onkeydown = (e) => {
            if (e.keyCode === 37 || e.keyCode === 100) {
                this.moveLeft();
            } else if (e.keyCode === 38 || e.keyCode === 88) {
                this.rotateRight();
            } else if (e.keyCode === 87) {
                this.rotateLeft();
            } else if (e.keyCode === 81) {
                this.rotate180();
            } else if (e.keyCode === 39 || e.keyCode === 102) {
                this.moveRight();
            } else if (e.keyCode === 40 || e.keyCode === 98) {
                this.moveDown();
            } else if (e.keyCode === 32 || e.keyCode === 104) {
                this.fall();
            }
        };
    }
    createPiece() {
        this.currentPiece = Piece.pieceToPlay(this.nextPiece);
        this.currentPiece.draw(this.ctx);
        this.nextPiece = Piece.getRandomPiece();
    }
    createNextPiece() {
        this.nextCtx.clearRect(0, 0, 4 * BLOCK_SIZE, 4 * BLOCK_SIZE);
        this.nextPiece.draw(this.nextCtx);
    }
    draw() {
        this.ctx.clearRect(0, 0, COLS * BLOCK_SIZE, ROWS * BLOCK_SIZE);
        this.grille();
        this.currentPiece.draw(this.ctx);
        this.drawFixedPieces();
    }
    drawFixedPieces() {
        for (let i = 0; i < ROWS; i++) {
            for (let j = 0; j < COLS; j++) {
                if (this.fixedPieces[i][j]) {
                    this.ctx.fillStyle = this.fixedPieces[i][j];
                    this.ctx.fillRect(j * BLOCK_SIZE, i * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
                }
            }
        }
    }
    moveLeft() {
        if (this.isPause) {
            return;
        }
        if (this.currentPiece.canLeft(this.fixedPieces)) {
            this.currentPiece.left();
            this.draw();
        }
    }
    moveRight() {
        if (this.isPause) {
            return;
        }
        if (this.currentPiece.canRight(this.fixedPieces)) {
            this.currentPiece.right();
            this.draw();
        }
    }
    moveDown() {
        if (this.isPause) {
            return;
        }
        if (this.currentPiece.canDown(this.fixedPieces)) {
            this.currentPiece.down();
            this.draw();
        } else {
            this.fixeAndCheck();
        }
    }
    fixeAndCheck() {
        this.fixed();
        this.createPiece();
        this.createNextPiece();
        this.checkClear();
        if (this.checkGameOver()) {
            this.gameOverFn();
            return;
        }
        if (this.checkWin()) {
            this.win();
            return;
        }
    }
    rotateRight() {
        if (this.isPause) {
            return;
        }
        if (this.currentPiece.canRotateRight(this.fixedPieces)) {
            this.currentPiece.rotateRight();
            this.draw();
        }
    }
    rotateLeft() {
        if (this.isPause) {
            return;
        }
        if (this.currentPiece.canRotateLeft(this.fixedPieces)) {
            this.currentPiece.rotateLeft();
            this.draw();
        }
    }
    rotate180() {
        if (this.isPause) {
            return;
        }
        if (this.currentPiece.canRotateRight(this.fixedPieces) && this.currentPiece.canRotateLeft(this.fixedPieces)) {
            this.currentPiece.rotateRight();
            this.currentPiece.rotateRight();
            this.draw();
        }
    }
    fall() {
        if (this.isPause) {
            return;
        }
        while (this.currentPiece.canDown(this.fixedPieces)) {
            this.currentPiece.down();
        }
        this.draw();
        this.fixeAndCheck();
    }
    fixed() {
        this.currentPiece.blocks.forEach((block) => {
            this.fixedPieces[block.y][block.x] = block.color;
        });
    }
    checkClear() {
        let clearCount = 0;
        for (let i = 0; i < ROWS; i++) {
            if (this.isRowFull(i)) {
                this.clearRow(i);
                clearCount++;
            }
        }
        if (clearCount > 0) {
            this.addScore(clearCount);
        }
    }
    isRowFull(row) {
        for (let i = 0; i < COLS; i++) {
            if (!this.fixedPieces[row][i]) {
                return false;
            }
        }
        return true;
    }
    clearRow(row) {
        for (let i = row; i > 0; i--) {
            for (let j = 0; j < COLS; j++) {
                this.fixedPieces[i][j] = this.fixedPieces[i - 1][j];
            }
        }
        for (let i = 0; i < COLS; i++) {
            this.fixedPieces[0][i] = null;
        }
        this.draw();
    }
    addScore(clearCount) {
        switch (clearCount) {
            case 1:
                this.scoreValue += 10;
                break;
            case 2:
                this.scoreValue += 30;
                break;
            case 3:
                this.scoreValue += 60;
                break;
            case 4:
                this.scoreValue += 100;
                break;
            default:
                break;
        }
        this.score.innerHTML = this.scoreValue;
        this.linesValue += clearCount;
        this.lines.innerHTML = this.linesValue;
    }
    checkGameOver() {
        for (let i = 0; i < COLS; i++) {
            if (this.fixedPieces[0][i]) {
                return true;
            }
        }
        return false;
    }
    gameOverFn() {
        clearInterval(this.timer);
        this.isGameOver = true;
        this.pause();
        this.ctx.fillStyle = '#000';
        this.ctx.fillRect(0, 0, COLS * BLOCK_SIZE, ROWS * BLOCK_SIZE);
        this.ctx.font = '40px Arial';
        this.ctx.fillStyle = '#fff';
        this.ctx.textAlign = 'center';
        this.ctx.textBaseline = 'middle';
        this.ctx.fillText('Game Over', COLS * BLOCK_SIZE / 2, ROWS * BLOCK_SIZE / 2);
        // restart
        var restart = document.getElementById('start');
        restart.style.display = 'block';

    }
    checkWin() {
        if (this.scoreValue >= 404) {
            return true;
        }
        return false;
    }
    win() {
        clearInterval(this.timer);
        this.isGameOver = true;
        this.pause();
        this.ctx.fillStyle = '#000';
        this.ctx.fillRect(0, 0, COLS * BLOCK_SIZE, ROWS * BLOCK_SIZE);
        this.ctx.font = '40px Arial';
        this.ctx.fillStyle = '#fff';
        this.ctx.textAlign = 'center';
        this.ctx.textBaseline = 'middle';

        var tetris = document.getElementById('container');
        tetris.style.display = 'none';

        var win = document.getElementById('win');
        win.style.display = 'block';

        document.getElementById('score_win').innerHTML = this.scoreValue;
    }
}