## Contributeurs :
# Groupe: All Clear

Valentin MONNET
Valentin RUSSEIL
Enzo BRAS
Cyprien TOURAINE
Damien RABIER

## Comment installer :

À la racine du projet faites les commandes : <br>
    pip install -r requirements.txt<br>
    python3 ./manage.py makemigrations<br>
    python3 ./manage.py migrate<br>
    python3 ./manage.py runserver --insecure<br>

Enfin votre site se trouvera à l'adresse http://localhost:8000/index/

## Défis inscrits et leurs solutions  :

# À la conquête de "l’œuf de pâques"

    L'oeuf de pâques se situe sur la carte dans la page http://localhost:8000/index/ où l'on peut voir le 7eme continent au milieu de l'atlantique sud.

# Où est Charlie ?

    Défi non effectué.

# 404 Tetris

    Lorsque l'on n'est pas sur la http://localhost:8000/index/ la page 'error 404' modifié s'affiche ou l'on peut jouer au tetris.

# e[sport]aster egg

    Défi non effectué.